from PyPDF2 import PdfFileWriter, PdfFileReader
from pathlib import Path

pdf_path = (
    Path.home() /
    "Desktop" /
    "Python_book" /
    "Pride_and_Prejudice.pdf" 
)

pdf_reader = PdfFileReader(str(pdf_path))

part1_writer = PdfFileWriter()
part2_writer = PdfFileWriter()

part1_pages = pdf_reader.pages[:150]  
part2_pages = pdf_reader.pages[150:]

for page in part1_pages:
    part1_writer.addPage(page)

part1_output_path = Path.home() / "part_1.pdf"
with part1_output_path.open(mode="wb") as part1_output_file:
    part1_writer.write(part1_output_file)


for page in part2_pages:
    part2_writer.addPage(page)


part2_output_path = Path.home() / "part_2.pdf"
with part2_output_path.open(mode="wb") as part2_output_file:
    part2_writer.write(part2_output_file)
