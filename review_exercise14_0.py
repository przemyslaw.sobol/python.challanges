from PyPDF2 import PdfFileReader
from pathlib import Path

pdf_path = (
    Path.home() /
    "Desktop" /
    "Python_book" /
    "zen.pdf" 
)

pdf_reader = PdfFileReader(str(pdf_path))

num_pages = pdf_reader.getNumPages()
print(num_pages)

first_page = pdf_reader.getPage(0)

text = first_page.extractText()

print(text)