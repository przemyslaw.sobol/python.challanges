import random

def coin_flip():
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"


num_trials = 10_000
flips = 0

for trial in range(num_trials):
    if coin_flip() == "heads":
        flips = flips + 1
        while coin_flip() == "heads":
            flips = flips + 1
        flips = flips + 1
    else:
        flips = flips + 1
        while coin_flip() == "tails":
            flips = flips + 1
        flips = flips + 1

average = flips / num_trials
print(f"The average number of flips per trial is {average}")