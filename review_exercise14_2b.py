from PyPDF2 import PdfFileWriter, PdfFileReader
from pathlib import Path

pdf_path = (
    Path.home() /
    "Desktop" /
    "Python_book" /
    "Pride_and_Prejudice.pdf" 
)

input_pdf = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()

for page in input_pdf.pages[::2]:
    pdf_writer.addPage(page)


with Path("every_other_page.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)
