while True:
    try:
        num = int(input('Enter a number: '))
        print(int(num))
        break
    except ValueError:
        print('Try again')

string_input = str(input('Enter some text: '))

try:
    n = int(input('Enter an integer: '))
    print(string_input[n])
except ValueError:
    print('That was not an integer')
except IndexError:
    print('That integer is out of bounds')