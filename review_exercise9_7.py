captians = {}
captains = dict(captians)

captains["Enterprise"] = "Picard"
captains["Voyager"] = "Janeway"
captains["Defiant"] = "Sisko"

if "Enterprise" not in captains:
    captains["Enterprise"] = "unknown"

if "Discovery" not in captains:
    captains["Discovery"] = "unknown"

for captain, ship in captains.items():
    print(f"The {ship} is captained by {captain}")


del captains["Discovery"]

print(captains)
