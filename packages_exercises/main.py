# main.py

from helpers.string import shout
from helpers.math import area

shout(f"the area of 5-by-8 rectangle is {area(5, 8)}")
