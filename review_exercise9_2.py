food = ["rice", "beans"]
food.append("broccoli")
food.extend(["bread", "pizza"])

print(food[0:2])
print(food[-1])

components = "eggs, fruit, orange, juice"
breakfast = components.split(", ")
print(breakfast)

print(len(breakfast) == 3)

lengths = [len(lenght) for lenght in breakfast]

print(lengths)