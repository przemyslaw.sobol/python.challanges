from pathlib import Path
import shutil

new_dir = Path.home() / "my_folder"
new_dir.mkdir()

file1 = new_dir / "file1.txt"
file2 = new_dir / "file2.txt"
image1 = new_dir / "image1.png"

file1.touch()
file2.touch()
image1.touch()

images_dir = new_dir / "images"
images_dir.mkdir()
image1.replace(images_dir / "image1.png")


file1.unlink()



shutil.rmtree(new_dir)