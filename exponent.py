num1 = float(input("Enter a base: "))
num2 = float(input("Enter an exponent: "))

result1 = num1 ** num2

print(f'{num1} to the power of {num2} = {result1}')