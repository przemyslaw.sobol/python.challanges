class Dog:
	species = "Canis familiaris"

	def __init__(self, name, age, coat_color):
		self.name = name
		self.age = age
		self.coat_color = coat_color


	def description(self):
            return f"{self.name} is {self.age} years old"



philo = Dog("Philo", 5, "brown")
print(f"{philo.name}'s coat is {philo.coat_color}.")

class Car:

	def __init__(self, color, mileage):
		self.color = color
		self.mileage = mileage
	
	def description(self):
		return f"The {self.color} car has {self.mileage} miles"

	def drive(self, miles):
		 self.mileage = self.mileage + miles


alpine = Car("blue", 20000)
ferrari = Car("red", 30000)

print(alpine.description())
print(ferrari.description())

redbull = Car("blue", 0)
redbull.drive(100)

print(redbull.description())