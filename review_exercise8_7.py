import random

def roll():
    return random.randint(1,6)

print(roll())

num_rolls = 10_000
total = 0

for trial in range(num_rolls):
     total = total + roll()

average = total / num_rolls

print(f"The average result of {num_rolls} is {average}")