import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]

verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
"explodes", "curdles"]

adjectives = ["furry", "balding", "incredulous", "fragrant",
"exuberant", "glistening"]

prepositions = ["against", "after", "into", "beneath", "upon",
"for", "in", "like", "over", "within"]

adverbs = ["curiously", "extravagantly", "tantalizingly",
"furiously", "sensuously"]


noun1 = random.choice(nouns)
noun2 = random.choice(nouns)
noun3 = random.choice(nouns)

while noun1 == noun2:
    noun2 = random.choice(nouns)
while noun3 == noun1 or noun3 == noun2:
    noun3 = random.choice(nouns)


verb1 = random.choice(verbs)
verb2 = random.choice(verbs)
verb3 = random.choice(verbs)

while verb1 == verb2:
    verb2 = random.choice(verbs)
while verb3 == verb1 or verb3 == verb2:
    verb3 = random.choice(verbs)


adj1 = random.choice(adjectives)
adj2 = random.choice(adjectives)
adj3 = random.choice(adjectives)

while adj1 == adj2:
    adj2 = random.choice(adjectives)
while adj3 == adj2 or adj3 == adj1:
    adj3 = random.choice(adjectives)

prp1 = random.choice(prepositions)
prp2 = random.choice(prepositions)

while prp1 == prp2:
    prp2 = random.choice(prepositions)

adv1 = random.choice(adverbs)

vowels = ["a", "e", "i", "o", "y", "u"]



if adj1[0] in vowels:
    A_or_An = "An"
else:
    A_or_An = "A"

print(f"{A_or_An} {adj1} {noun1} \n\n{A_or_An} {adj1} {noun1} {verb1} {prp1} the {adj2} {noun2} \n{adv1}, the {noun1} {verb2}\nthe {noun2} {verb3} {prp2} a {adj3} {noun3}")
