def cube(x):
    """Return the cube of the input number"""
    cubenum = x **3
    return cubenum

print(cube(2))
print(cube(3))
print(cube(10))

def greet(name):
    """Display a greeting"""
    greeting = f'Hello {name}!'
    return greeting

print(greet('Przemek'))