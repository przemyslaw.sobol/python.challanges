def invest(amount, rate, years):
    """Display year on year growth of an initial investment"""
    for year in range(years):
        amount = amount + amount * rate
        print(f'year {year + 1}: ${amount:.2f}')

amount = float(input("Enter a principal amount: "))
rate = float(input("Enter an anual rate of return: "))
years = int(input("Enter a number of years: "))


invest(amount, rate, years)