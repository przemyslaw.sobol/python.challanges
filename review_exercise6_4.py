for n in range(2,11):
    print(n)


n = 1
while n < 10:
    n = n + 1
    print(n)

def doubles(num):
    return num * 2


x = 2
for i in range(3):
    x = doubles(x)
    print(x)