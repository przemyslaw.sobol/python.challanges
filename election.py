from random import random

A_wins = 0
B_wins = 0

num_trials = 10_000

for trial in range(num_trials):
    votes_for_A = 0
    votes_for_B = 0

    if random() < 0.87:
        votes_for_A = votes_for_A + 1
    else:
        votes_for_B = votes_for_B + 1

    if random() < 0.65:
        votes_for_A = votes_for_A + 1
    else:
        votes_for_B = votes_for_B + 1

    if random() < 0.17:
        votes_for_A = votes_for_A + 1
    else:
        votes_for_B = votes_for_B + 1
    
    if votes_for_A > votes_for_B:
        A_wins = A_wins + 1
    else:
        B_wins = B_wins + 1


print(f"Probability of A wins: {A_wins / num_trials}")
print(f"Probability of B wins: {B_wins / num_trials}")