from PyPDF2 import PdfFileWriter, PdfFileReader
from pathlib import Path

pdf_path = (
    Path.home() /
    "Desktop" /
    "Python_book" /
    "Pride_and_Prejudice.pdf" 
)


input_pdf = PdfFileReader(str(pdf_path))
last_page = input_pdf.getPage(-1)

pdf_writer = PdfFileWriter()
pdf_writer.addPage(last_page)

with Path("last_page.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)
