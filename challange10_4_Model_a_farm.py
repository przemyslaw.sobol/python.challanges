class Animal:

    position = 0
    food_eaten = 0

    def __init__(self, name, color):
        self.name = name
        self.color = color
    
    def __str__(self):
        return f"{self.name} is {self.color}"

    def speak(self, sound=None):
        if sound is None:
            return f"{self.name} can't speak"
        return f"{self.name} says {sound}"
    
    def walk(self, steps):
        self.position = self.position + steps
        return self.position

    def feed(self):
        self.food_eaten = self.food_eaten + 1
        if self.food_eaten > 3:
            return self.poop()
        return f"{self.name} is eating"

    def poop(self):
        self.food_eaten = 0
        return f"{self.name} needs to find a bathroom"

class Cow(Animal):
    def speak(self, sound = "Mooooo"):
        return super().speak(sound)

class Horse(Animal):
    def speak(self, sound = "Ihaaa"):
        return super().speak(sound)

class Dog(Animal):
    def speak(self, sound = "Wof, wof"):
        return super().speak(sound)


ricci = Dog("Ricci", "spotted")
print(ricci)
print(ricci.speak())
print(ricci.walk(2))
print(ricci.position)
print(ricci.feed())
print(ricci.feed())
print(ricci.feed())
print(ricci.feed())