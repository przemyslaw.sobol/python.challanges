num = float(input("Enter a number: "))
roundnum = round(num, 2)

print(f'{num} rounded to 2 decimal places is {roundnum}')



num1 = float(input("Enter a number: "))
absolute1 = abs(num1)
print(f'The absolute value of {num1} is {absolute1}')

num2 = float(input("Enter a number: "))
num3 = float(input("Enter another number: "))

print(
    f"The difference between {num1} and {num2} is an integer? "
    f"{(num1 - num2).is_integer()}!"
)