data = ((1, 2), (3, 4))

for i in range(len(data)):
    print(f"Row {i + 1} sum: {data[i][0] + data[i][1]}")


numbers = [4, 3, 2, 1]
copynumbers = numbers[:]
numbers.sort()

print(numbers)
print(copynumbers)