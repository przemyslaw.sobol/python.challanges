import random

capitals_dict = {
'Alabama': 'Montgomery',
'Alaska': 'Juneau',
'Arizona': 'Phoenix',
'Arkansas': 'Little Rock',
'California': 'Sacramento',
'Colorado': 'Denver',
'Connecticut': 'Hartford',
'Delaware': 'Dover',
'Florida': 'Tallahassee',
'Georgia': 'Atlanta',
}

rand_state = random.choice(list(capitals_dict))

enter_capital = input(f"Enter the capital of: {rand_state}: ")

while True:
    if enter_capital.lower() == "exit":
        print(f"The answer is {capitals_dict[rand_state]}. Goodbye!")
        break
    elif enter_capital.lower() != capitals_dict[rand_state].lower():
        enter_capital = input(f"Enter the capital of: {rand_state}: ")
    elif enter_capital.lower() == capitals_dict[rand_state].lower():
        print("Correct")
        break
    
